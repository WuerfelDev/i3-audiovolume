# i3-audiovolume

**Since i3 config v4 it seems they included a few lines for the control, so this is no longer needed**

Use media keys to change current volume of audio output. eg for Bluetooth audio sink
<br><br>
Set it up, it is really simple:<br><br>

1. Clone audiovolume.sh to your computer
2. Add lines like these from my config file to your ~/.config/i3/config<br>https://gitlab.com/WuerfelDev/i3-audiovolume/blob/master/config
#!/bin/sh

# Copyright 2018 WuerfelDev
# Licensed under MIT License


# Get index of current active sink
SINK=$( pactl list sinks short | grep RUNNING | sed -e 's,^\([0-9][0-9]*\)[^0-9].*,\1,' )
# If the sink is SUSPENDED
if [ -z "$SINK" ]
then
	SINK="0"
fi
if [ $1 = "mute" ]
then
	# toggle mute status if parameter is mute
	# pactl -- set-sink-mute $SINK toggle
	amixer set Master toggle
else
	# Turn mute off
	pactl -- set-sink-mute $SINK false
	
	# just use parameter to set volume,
	# this is vol+ and also vol-	
	pactl -- set-sink-volume $SINK $1
	
	# send notification about current volume
	NOW=$( pactl list sinks | grep '^[[:space:]]Volume:' | head -n $(( $SINK + 1 )) | tail -n 1 | sed -e 's,.* \([0-9][0-9]*\)%.*,\1,' )

	# under 100% normal, else critical notification
	if [ "$NOW" -lt "100" ]
	then
		notify-send -t 400 "Audiovolume is set to $NOW%"
	else
		notify-send -t 400 -u critical "Audiovolume is set to $NOW%"
	fi
fi